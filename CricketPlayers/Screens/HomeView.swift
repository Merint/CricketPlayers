

import SwiftUI

struct HomeView: View {
   
    var players:[Player] = PlayersData
    let color:Color
   
    init(color:Color = .gray){
        self.color = color
    }
    
    var body: some View {
        NavigationView{
            List{
                ForEach(players) { player in
                    NavigationLink(destination:PlayerDetailView(player: player)){
                        PlayersListView(player:player)
                    }
                   
                    line
                }
            }
            .navigationBarTitle("Players")
           
        }
       
        
        
        
    }
    
    
    //struct HomeView_Previews: PreviewProvider {
    //    static var previews: some View {
    //        HomeView()
    //    }
    
    var line:some View {
        VStack{Divider().frame(width:400,height: 2).background(color)}
    }
}
