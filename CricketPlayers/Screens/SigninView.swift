//
//  SigninView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 16/11/22.
//

import SwiftUI
  

struct SigninView: View {
    @State private var username = ""
    @State private var password = ""
    @State private var wrongPassword = 0
    @State private var wrongUsername = 0
    @State private var showingLoginScreen = false
    var body: some View {
        NavigationView{
            ZStack{
                Color.blue
                    .ignoresSafeArea()
                Circle()
                    .scale(1.7)
                    .foregroundStyle(.white.opacity(0.15))
                Circle()
                    .scale(1.35)
                    .foregroundStyle(.white)
                VStack{
                    Text("Login")
                        .font(.largeTitle)
                        .bold()
                        .padding()
                    TextField("Username", text:$username)
                        .padding()
                        .frame(width:300, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongUsername))
                    
                    SecureField("Password", text:$password)
                        .padding()
                        .frame(width:300, height:50)
                        .background(Color.black.opacity(0.05))
                        .cornerRadius(10)
                        .border(.red, width: CGFloat(wrongPassword))
                        . padding()
//                    Button("Login"){
//                        authenticateUser(username: username, password: password)
//                    }
//                    .foregroundColor(.white)
//                    .frame(width: 250, height:50)
//                    .background(Color.blue)
//                    .cornerRadius(50)
                    VStack{
                        NavigationLink(destination: BaseView()){
//                            EmptyView()
                            Button("Login"){
                                authenticateUser(username: username, password: password)
                            }
                            .foregroundColor(.white)
                            .frame(width: 250, height:50)
                            .background(Color.blue)
                            .cornerRadius(50)
                        }
                    }
//                                   Text("You are logged in @\(username)"), isActive: $showingLoginScreen)
               }
            }
        }.navigationBarHidden(true)
    }
    func authenticateUser(username: String,password:String){
        if (username == "") {
            wrongUsername = 2
            
        }
        if (password == "") {
            wrongPassword = 2
        }
    }
}

struct SigninView_Previews: PreviewProvider {
    static var previews: some View {
        SigninView()
    }
}
