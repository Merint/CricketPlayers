//
//  GalleryView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 14/11/22.
//

import SwiftUI

struct GalleryView: View {
    
    var players:[Player] = PlayersData
    @State var selectedIndex: Int = 0
    @State var selectedPlayer: Player = PlayersData[0]
    @State var scale = 1.0
    var gridLayout: [GridItem] = [GridItem.init(.flexible())]
//    var player:Player
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false ){
                VStack {
                    ScrollView(.horizontal, showsIndicators: false ){
                        HStack(alignment:.top,spacing: 8){
                            ForEach(players.indices) { index in
                                VStack{
                                    Image(players[index].image)
                                        .resizable()
                                        .scaledToFit()
                                        .background(Color.accentColor)
                                        .frame(width: 79,height: 90,alignment: .center)
                                        .cornerRadius(50)
                                        .overlay(Circle().stroke(Color.primary, lineWidth:
                                                        selectedIndex == index ? 8 :1))
                                        .clipShape(Circle())
                                        .animation(.easeIn)
                                    
                                    
                                        .onTapGesture {
                                            selectedIndex = index
                                            selectedPlayer = players[index]
                                        }
                                    Text(players[index].name)
                                        .fontWeight(.semibold)
                                        .foregroundColor(.accentColor)
                                        
                                }
                            }
                        }
                        .padding(.vertical,10)
                        .padding(.horizontal,10)
                    }
                    LazyVGrid(columns:gridLayout, alignment: .center, spacing: 10){
                        ForEach(selectedPlayer.gallery, id:\.self) { galleryImage in
                            Image(galleryImage)
                                .resizable()
                                .scaledToFit()
                                .cornerRadius(8)
                            
                        }
                       
                    }
                    .padding(.horizontal, 10)
                    .animation(.easeIn)
                }
                .padding(.vertical,10)
            }
            .frame(maxWidth: .infinity,maxHeight: .infinity)
            .navigationBarTitle("Gallery", displayMode: .inline)
            
        }
        
    }
}

struct GalleryView_Previews: PreviewProvider {
    static var previews: some View {
        GalleryView()
    }
}
