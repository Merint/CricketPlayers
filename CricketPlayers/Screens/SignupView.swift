//
//  SignupView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 15/11/22.
//

import SwiftUI

struct SignupView: View {
    var body: some View {
      
        NavigationView{
        ZStack{
            Color("grey").edgesIgnoringSafeArea(.all)
            VStack{
                Text("Sign Up")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                
                HStack{
                    Image("Apple")
                        .resizable()
                        .frame(width: 40,height: 40)
                        .padding(.horizontal)
                    Text("Sign in with Apple")
                        .font(.title2)
                    
                }
                .padding()
                .frame(width: 375,height: 60)
                .background(Color.white)
                .cornerRadius(50)
                .padding(.vertical)
                HStack{
                    Image("Google")
                        .resizable()
                        .frame(width: 40,height: 40)
                        .padding(.horizontal)
                    VStack{
                        NavigationLink(destination: BaseView())
                        {
                            Text("Sign in with Google")
                                .font(.title2)
                                .foregroundColor(.pink)
                        }}
                }
                .padding()
                .frame(width: 375,height: 60)
                .background(Color.white)
                .cornerRadius(50)
                .padding(.vertical)
                
                Text("or get a link emailed to you")
                    .foregroundColor(.gray)
                    .padding()
                HStack{
                    Text("Work email address")
                        .font(.title3)
                        .foregroundColor(.gray)
                    
                }
                .frame(width: 375,height: 60)
                .background(Color.white)
                .cornerRadius(50)
                HStack{
                    
                    Text("Email me a signup link")
                        .font(.title2)
                        .foregroundColor(.white)
                        .fontWeight(.semibold)
                    
                }
                .padding()
                .frame(width: 375,height: 60)
                .background(Color.pink)
                .cornerRadius(50)
                .padding(.vertical)
                
                line
                    .padding(.vertical)
                HStack{
                    Text("You are completely safe.")
                    
                }
                HStack{
                    
                    Text("Read our Terms & Conditions")
                        .foregroundColor(.pink)
                }
            }
            .padding()
            
        }
           
        }.navigationBarHidden(true)
        
    }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        SignupView()
    }
}

var line:some View {
    VStack{Divider().frame(width:400,height: 0.5)
        .background(Color.gray)
        .padding(.vertical)
    }
}
