//
//  WelcomeView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 15/11/22.
//

import SwiftUI

struct WelcomeView: View {
    var body: some View {
        NavigationView{
            ZStack {
                Color("grey").edgesIgnoringSafeArea(.all)
                VStack{
                    Spacer()
                    Image("Criket")
                    Spacer()
                    Text("Get Started")
                        .font(.title3)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(width: 300,height: 30)
                        .padding()
                        .background(Color("blue"))
                        .cornerRadius(50)
                    
                    VStack{
                        NavigationLink(destination: SigninView())
                        {
                        Text("Sign In")
                            .font(.title3)
                            .fontWeight(.bold)
                            .frame(width: 300,height: 20)
                            .padding()
                            .background(Color(.white))
                            .cornerRadius(50)
                            .padding(.vertical)
                            
                        }
                    }
                    
                    //                NavigationLink(destination: BaseView(){
                    //
                    //                }
                    HStack{
                        Text("New around here")
                        NavigationLink(destination: SignupView())
                        {
                            Text("Sign up")
                                .foregroundColor(Color("blue"))
                        }
                    }
                }
            }
           
        }
        .background(Color(.red))
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}

