//
//  PlayerDetailView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 11/11/22.
//

import SwiftUI
import WebKit

struct PlayerDetailView: View {
    var player:Player

    var body: some View {
        ScrollView(.vertical,showsIndicators: false){
            VStack(alignment: .center,spacing: 20){
                Image(player.gallery.randomElement() ?? "")
                    .resizable()
                    .scaledToFit()
                Text(player.name.uppercased())
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundColor(.accentColor)
                GroupBox(){
                    HStack{
                        Group{
                            Image(systemName: "person.fill")
                            Text("Role:")
                                .font(.footnote)
                                .fontWeight(.semibold)
                            Spacer()
                            Text(player.role)
                                .font(.footnote)
                        }
                    }
                    Divider()
                    HStack{
                        Group{
                            Image(systemName: "calendar")
                            Text("Date of birth")
                                .font(.footnote)
                                .fontWeight(.semibold)
                            Spacer()
                            Text(player.dob)
                                .font(.footnote)
                        }
                    }
                }
                .padding(.horizontal)
                Group{
                    HeaderView(iconName: "photo.on.rectangle",title:"Gallery")
                    ScrollView(.horizontal,showsIndicators:false){
                        HStack{
                            ForEach(player.gallery, id:\.self) { imageName in
                                Image(imageName)
                                    .resizable()
                                    .scaledToFit()
                                    .frame(height: 200)
                                .cornerRadius(10)
                            }
                        }
                    }
                }
                .padding(.horizontal)
                Group{
                    HeaderView(iconName: "info.circle",title:"About \(player.name)")
                    Text(player.description)
                        .multilineTextAlignment(.leading)
                        .font(.body)
                        .layoutPriority(1)
                }
                .padding(.horizontal)
            Group{
                HeaderView(iconName: "link",title:"More Info")
                GroupBox(){
                    HStack{
                        Group{
                         Image(systemName: "globe")
                            Text("Read More:")
                                .font(.footnote)
                                .fontWeight(.semibold)
                            Spacer()
                            Image(systemName: "arrow.up.right.square")
                                .foregroundColor(.accentColor)
                            Link("Wikipedia",destination: URL.init(string:player.wikilink)!)
                                .font(.footnote)
                                .foregroundColor(.accentColor)
                            
                        }
                       
                    }
                }
                VStack{
                    VideoView(videoID:player.videoid)
                        .frame(width:300, height:150,alignment:.leading)
                        .cornerRadius(12)
                        .padding()
                }
            }
            .padding(.horizontal)
            }
            .navigationBarTitle("Player Details",displayMode:.inline)
            .navigationBarHidden(true)
        }
        
        
    }
    
    
}

struct VideoView : UIViewRepresentable {
    let videoID : String
   
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func  updateUIView(_ uiView: WKWebView,context: Context){
        guard  let youtubeURL = URL(string:"https://www.youtube.com/watch?v=\(videoID)")else{return}
        uiView.scrollView.isScrollEnabled = false
        uiView.load(URLRequest(url: youtubeURL))
      
    }
}


struct PlayerDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerDetailView(player : PlayersData[4])
    }
}
