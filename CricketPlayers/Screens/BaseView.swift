//
//  BaseView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 14/11/22.
//

import SwiftUI

struct BaseView: View {
    var body: some View {
        TabView{
            HomeView()
                .tabItem{
                    Image(systemName: "person.3")
                    Text("Players")
                        .font(.footnote)
                        .fontWeight(.heavy)
                }
            GalleryView()
                .tabItem{
                    Image(systemName: "photo.on.rectangle.angled")
                    Text("Gallery")
                        .font(.footnote)
                        .fontWeight(.heavy)
                }
            
        }
    }
}

struct BaseView_Previews: PreviewProvider {
    static var previews: some View {
        BaseView()
       
    }
}
