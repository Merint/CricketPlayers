//
//  Player.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 10/11/22.
//

import Foundation

struct Player: Identifiable{
    var id = UUID()
    var name: String
    var role:String
    var dob:String
    var image:String
    var description:String
    var gallery:[String]
    var wikilink: String
    var videoid: String
}
