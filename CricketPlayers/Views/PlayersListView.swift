//
//  PlayersListView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 10/11/22.
//

import SwiftUI

struct PlayersListView: View {
    
    var player:Player
    var body: some View {
        HStack{
            Image(player.image)
             .resizable()
             .scaledToFit()
             .frame(width:80,height:80,alignment: .center )
             .shadow(color:.black,radius:5, x:5,y:5)
             .background(Color.accentColor)
             .cornerRadius(8)
            VStack(alignment: .leading, spacing: 8){
                Text(player.name)
                    .font(.title2)
                    .fontWeight(.bold)
                Text(player.role)
                    .font(.body)
                    .foregroundColor(.secondary)
            }
            
        }
    }
}

struct PlayersListView_Previews: PreviewProvider {
    static var previews: some View {
        PlayersListView(player: PlayersData[2])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
