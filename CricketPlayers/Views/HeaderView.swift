//
//  HeaderView.swift
//  CricketPlayers
//
//  Created by Merin K Thomas on 11/11/22.
//

import SwiftUI

struct HeaderView: View {
    
    var iconName = ""
    var title = ""
    
    var body: some View {
        HStack {
            Image(systemName: iconName)
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text(title)
                .fontWeight(.bold)
                .font(.title3)
        }
        .padding(.vertical)
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
